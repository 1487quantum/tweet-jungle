# tweet-jungle
<img src="https://raw.githubusercontent.com/1487quantum/tweet-jungle/master/tweet_jungle.png" width="75%"></img>
<p>Source code for the Tweet Jungle created during the NUS Hack&Roll 2016 Hackathon.</p>
More info can be found here:
- http://cyaninfinite.com/events/nus-hack-roll-hackathon-2016/
- http://devpost.com/software/read-that-tweet

###Software:
- Raspbian Wheezy (Linux)
- Python
- Microsoft Project Oxford TTS
- Twitter API

###Hardware:
- Raspberry Pi 2
- I2C OLED 0.96" Display
- USB Speaker
- Jumper wires

###Schematic
<img src="https://raw.githubusercontent.com/1487quantum/tweet-jungle/master/tj_schematics.png"></img>
