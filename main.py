import sys
sys.path.append('/usr/local/lib/python2.7/dist-packages/')		#Append path accordingly
import http.client, urllib.parse, json	#For http request
import os								#To play audio
import time
import re								#To remove unnecessary stuff
from langdetect import detect			#To detect language
from twython import TwythonStreamer		#Get twitter api
from random import randint				#Random Generator
from lib_oled96 import ssd1306			#I2C 0.96" OLED Display
from smbus import SMBus					
from PIL import ImageFont, ImageDraw, Image		#Graphic library

transWd = ['Next up', 'In other stories', 'Next', 'Looking at other stories', 'Going on', 'Going on next']	#Transition words

i2cbus = SMBus(1)        # 1 = Raspberry Pi but NOT early REV1 board
oled = ssd1306(i2cbus)   # create oled object, nominating the correct I2C bus, default address

# Search terms
TERMS = ['#news']
FOLLOW = ['BBCBreaking','breakingviews']

ttsHost = "https://speech.platform.bing.com"			#TTS Host

# Twitter application authentication
APP_KEY = '<API KEY here>'
APP_SECRET = '<API SECRET here>'
OAUTH_TOKEN = '<ACCESS TOKEN here>'
OAUTH_TOKEN_SECRET = '<ACCESS TOKEN SECRET here>'

def getSpeech(msg):
	#-------------------HTTP REQUEST---------------------------
	#Note: Sign up at http://www.projectoxford.ai to get a subscription key.  
	#Search for Speech APIs from Azure Marketplace.
	#Use the subscription key as Client secret below.
	clientId = "nothing"
	clientSecret = "6e2afb5e05c6446fb63fe32da1bbe058"

	params = urllib.parse.urlencode({'grant_type': 'client_credentials', 'client_id': clientId, 'client_secret': clientSecret, 'scope': ttsHost})

	#print ("The body data: %s" %(params))

	headers = {"Content-type": "application/x-www-form-urlencoded"}
				
	AccessTokenHost = "oxford-speech.cloudapp.net"
	path = "/token/issueToken"

	# Connect to server to get the Oxford Access Token
	conn = http.client.HTTPSConnection(AccessTokenHost)
	conn.request("POST", path, params, headers)
	response = conn.getresponse()
	print(response.status, response.reason)

	data = response.read()
	#print(data)
	conn.close()
	accesstoken = data.decode("UTF-8")
	#print ("Oxford Access Token: " + accesstoken)

	#decode the object from json
	ddata=json.loads(accesstoken)
	access_token = ddata['access_token']

	#Filtering hashtags
	#Get hashtags
	exp = r"#(\w+)"	#Regex search
	expR = r"(#(\w+))"	#Regex search for filtering hash
	rep = r"\2"	#Replacement
	pat = re.compile(exp)	
	tags = pat.findall(msg)		#Get all keywords
	#print(re.sub(expR, rep, msg))
	print(tags)
	
	#Add period before first hashtag
	#msg = re.search(r"^[^#]+(?= )", msg, re.IGNORECASE).group(1)+'.'
	msg = re.sub(r'(^[^#]+(?=))(\s#.*)', r'\1.\n\2', msg)
	
	#Neutralise hashtag
	text = re.sub(expR, rep, msg)
	
	#Add transition words
	text = transWd[randint(0,5)]+', '+ text
	print('>>>>>>',text,'\n')

	#Body of request
	body = "<speak version='1.0' xml:lang='en-us'><voice name='Microsoft Server Speech Text to Speech Voice (en-US, BenjaminRUS)'>"+text+"</voice></speak>"

	headers = {"Content-type": "application/ssml+xml", 
				"X-Microsoft-OutputFormat": "riff-16khz-16bit-mono-pcm", 
				"Authorization": "Bearer " + access_token, 
				"X-Search-AppId": "07D3234E49CE426DAA29772419F436CA", 
				"X-Search-ClientID": "1ECFAE91408841A480F00935DC390960", 
				"User-Agent": "TTSForPython"}
				
	#Connect to server to synthesize the wave
	conn = http.client.HTTPSConnection("speech.platform.bing.com:443")
	conn.request("POST", "/synthesize", body, headers)
	response = conn.getresponse()
	#print(response.status, response.reason)
	data = response.read()
	f = open('speech.wav','wb')
	f.write(data)
	f.close()
	conn.close()
	#print("The synthesized wave length: %d" %(len(data)))

	oled.cls()
	draw = oled.canvas
	e=0
	q=0
	for i in tags:
		if q==6:
			draw.text((32, e),    tags[q], fill=1)
		else:
			draw.text((1, e),    tags[q], fill=1)
		oled.display()
		e+=10
		q+=1
	#Play audio
	os.system('aplay speech.wav')
	print('\n')


	
# Setup callbacks from Twython Streamer
class BlinkyStreamer(TwythonStreamer):
    def on_success(self, data):
        if ('text' in data) and (detect(data['text'])=='en'):
            rmLinkRegex = re.sub(r"(?:\@|https?\://)\S+", '', data['text'], flags=re.MULTILINE)
            rmNonAscii = re.sub(r'[^\x00-\x7F]+',' ', rmLinkRegex)
            print(rmNonAscii)
            print('\n')
            print('*****',detect(data['text']),"\n")
            getSpeech(rmNonAscii)
            time.sleep(1)
			

stream = BlinkyStreamer(APP_KEY, APP_SECRET, OAUTH_TOKEN, OAUTH_TOKEN_SECRET)
stream.statuses.filter(track=TERMS)
